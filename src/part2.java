import org.json.*;

import java.util.*;
import java.lang.*; 
import java.io.*;
//by Jonathan Menzies 
//Key - AL means arraylist 
public class part2 {
	
	//flags for error messages 
	static int flag = 0;
	static int moreEQ = 0;
	static int undefined = 0;
	static int opflag = 0;
	static ArrayList <Name> answers = new ArrayList <Name>();
	
	//Methods created to remove duplicates from the tree structure, can only be run in certain places or it interferes
	//with set equality
	
	
	private static ArrayList <Set> byeSets(ArrayList <Set> set)
	{
		ArrayList <Set> f1 = set;
		
		//takes the inputed array list and compares every element to each other
		//if there is a duplicate it is removed
		for(int aw = 0;aw < f1.size();aw++)
		{
			ArrayList <Set> s1 = new ArrayList <Set>();
			
			if(f1.get(aw).getType().equals("set"))
			{
				s1 = (f1.get(aw).getSet());
			}
			if(f1.get(aw).getType().equals("int"))
			{
				s1.add(new Set(f1.get(aw).getInt()));
			}
			if(f1.get(aw).getType().equals("pair2"))
			{
				s1.add(new Set(f1.get(aw).getPair2()));
			}
			
			boolean t = false;
			
			for(int q = 0;q < f1.size();q++)
			{
				ArrayList <Set> s2 = new ArrayList <Set>();
				if(q != aw)
				{
					if(f1.get(aw).getType().equals(f1.get(q).getType()))
					{
						if(f1.get(q).getType().equals("set"))
						{
							s2 = (f1.get(q).getSet());
						}
						if(f1.get(q).getType().equals("int"))
						{
							s2.add(new Set(f1.get(q).getInt()));
						}
						if(f1.get(q).getType().equals("pair2"))
						{
							s2.add(new Set(f1.get(q).getPair2()));
						}
						
						t = setEquality(s1,s2);
					}
					
					
				}
				if(t == true)
				{
					f1.remove(q);
					//System.out.println("true");
				}
			}
			
			
		}
			
			return f1;
		}
			
		
		
	
	//This is the method to parse the JSON file, it also runs methods for set equality 
	//and set membership
	private static ArrayList <Set> arguments(JSONArray y)throws IOException, JSONException
	{
		
		
			//creates a new JSON array to put the inputed one into
			JSONArray f = y;
			//flag for set equality
			int eq = 0;
			//boolean value for set quality
			boolean bb = false;
			//Creates an array list of Set(tree strucutre) to store everything in
			ArrayList <Set> f1 = new ArrayList <Set>() ;
			
			
			for(int k = 0; k < f.length();k++)
	    	{
				//if the object in the JSON Array is an integer then add it to the AL
		    	if(f.get(k) instanceof Integer)
		    	{
		    		int g = (int) f.get(k);
		        	//System.out.println(g);
		        	f1.add(new Set(g));
		        	
		    	}
		    	
		    	//if the element in the array is a JSONObject 
		    	if(f.get(k) instanceof JSONObject)
		    	{
		    		JSONObject i = (f.getJSONObject(k)); //retrieve JSONObject
		    		int counter = 0;//counter to check if variable is defined or not
		    		if(f.getJSONObject(k).has("variable") )//if the JSONObject is a variable
		    		{
		    			String a = i.getString("variable");//stores the variable from the JSONObject
		    			//System.out.println(a + "no");
		    			if(flag == 1)//if this is not the first time variable is seen
		    			{
		    				//System.out.println("yes");//Loop checks each element in the answers to 
		    				//see if the variable is contained- if it is there then it is added
		    				//to the AL, if not then undefined is printed
		    				for(int w = 0;w < answers.size(); w++)
		    				{
		    					//System.out.println("yes1");
		    					if(answers.get(w).getType().equals(a))
		    					{
		    						if(answers.get(w).getB().get(0).getType().equals("set"))
		    						{
		    							f1.add(new Set(answers.get(w).getB().get(0).getSet()));
		    						}
		    						
		    						if(answers.get(w).getB().get(0).getType().equals("int"))
		    						{
		    							f1.add(new Set(answers.get(w).getB().get(0).getInt()));
		    						}
		    						
		    						if(answers.get(w).getB().get(0).getType().equals("pair2"))
		    						{
		    							f1.add(new Set(answers.get(w).getB().get(0).getPair2()));
		    						}
		    						
		    						//System.out.println(answers.get(w).getB());
		    					}
		    					else
		    					{
		    						counter++;
		    					}
		    				}
		    				if(counter == answers.size())//if counter == the size of answers then the variable is not in it
		    				{
		    					 
		    					undefined++;
		    					//break;
		    						    
		    					    
		    				}
		    			}
		    		}
		    		flag = 1;
		    		if(f.getJSONObject(k).has("operator") )//if the JSONObject is operator then do the following
		    		{
		    			if(i.getString("operator").equals("member"))//if operator is member
		    			{
		    				JSONArray z = (i.getJSONArray("arguments"));//gets the JSONArray to parse
		    				boolean v = false;
		    				//retrieves the next elements
		    				ArrayList <Set> out = arguments(z);
		    				ArrayList <Set> member1 = new ArrayList <Set>();//creates empty AL
		    				ArrayList <Set> member2= new ArrayList <Set>();//creates empty AL
		    				
		    				//if statements take the returned AL from arguments and assign the elements 
		    				//to the empty AL's
		    				if(out.get(0).getType().equals("set"))
			    			{
		    					member1 = byeSets(out.get(0).getSet());
			    				
			    			}
			    				
			    			if(out.get(0).getType().equals("pair2"))
			    			{
			    				member1.add(new Set(out.get(0).getPair2()));
			    			}
			    				
			    			if(out.get(0).getType().equals("int"))
			    			{
			    				member1.add(new Set(out.get(0).getInt()));
			    			}
			    			
			    			if(out.get(1).getType().equals("set"))
			    			{
		    					member2 = byeSets(out.get(1).getSet());
			    				
			    			}
			    				
			    			if(out.get(1).getType().equals("pair2"))
			    			{
			    				member2.add(new Set(out.get(1).getPair2()));
			    			}
			    				
			    			if(out.get(1).getType().equals("int"))
			    			{
			    				member2.add(new Set(out.get(1).getInt()));
			    			}
			    			v = setMembership(member1,member2);//run method to determine if the element is in the set
			    			if(v == true)//if in set
			    			{
			    				f1.add(new Set(1));
			    			}
			    			else//if not in set
			    			{
			    				f1.add(new Set(0));
			    			}
		    			}
		    			else
		    			{
		    				opflag++;
		    			}
		    		
		    			if(i.getString("operator").equals("equal"))//if the operator is equal
		    			{
		    				//System.out.println(i.getString("operator"));
		    				eq++;//sets flag to tell program to perform set equality
		    				
			    			JSONArray z = (i.getJSONArray("arguments"));
			    			//System.out.println(z + "z");
			    			ArrayList <Set> out = arguments(z);//gets the next elements from JSON
			    			for(int ye = 0;ye < out.size();ye++)
			    			{
			    				//adds the elements from out to the AL 
			    				if(out.get(ye).getType().equals("set"))
			    				{
			    					f1.add(new Set(byeSets((out.get(ye).getSet()))));
			    					//System.out.println((out.get(ye).getSet()));
			    				}
			    				
			    				if(out.get(ye).getType().equals("pair2"))
			    				{
			    					f1.add(new Set(out.get(ye).getPair2()));
			    				}
			    				
			    				if(out.get(ye).getType().equals("int"))
			    				{
			    					f1.add(new Set(out.get(ye).getInt()));
			    				}
			    			}
			    			
			    				
			    			
		    			}
		    			else
		    			{
		    				opflag++;
		    			}
		    			
		    			if(i.getString("operator").equals("set"))//if operator is set
		    			{
		    				//takes JSONArray gets the elements then puts them into the AL
		    				
		    				JSONArray z = (i.getJSONArray("arguments"));
		    				ArrayList <Set> out = byeSets(arguments(z));
		    				f1.add(new Set(out));
		    				//System.out.println(z + "w");
		    				
		    			}
		    			else
		    			{
		    				opflag++;
		    			}
		    			if(i.getString("operator").equals("tuple"))//if operator is a tuple
		    			{
		    				//takes JSONArray and retrieves the elements in a AL
		    				JSONArray w = (i.getJSONArray("arguments"));
		    				ArrayList <Set> gh = new ArrayList<Set>();
		    				gh = arguments(w);
		    				//converts the AL from a set type to a tuple type (Pair)
		    				Pair2 [] tup = pairArguments(gh);
		    				//System.out.println(w);
		    				f1.add(new Set(tup));//adds the tuple to the set
		    			}
		    			else
		    			{
		    				opflag++;
		    			}
		    		}
		    		
		    	}
		    	
		    	
	    	}
			if(eq == 1)//if flag has been set to one then perform set equality
			{
				ArrayList <Set> s1 = f1.get(0).getSet();//takes first element from f1
				ArrayList <Set> s2 = f1.get(1).getSet();//takes second element from f1
				//System.out.println(s2.size() + "s2 size");
				//System.out.println(s1.size() + "s1 size");
				ArrayList <Set> blank = new ArrayList<Set>();//creates blank AL
				bb = setEquality(s1,s2);//runs set equality - if true then bb = true
				
				//System.out.println(s1.size() + "s1 size");
				//System.out.println(s2.size() + "s2 size");
				if(bb == true)//if the sets are equal
				{
					if(moreEQ == 1)//if this is not the first set equality in this function then simply remove the second element
					{              //if true
						f1.remove(1);
					}			//this is explained in my report - due to confusion with multiple set equality				
					else
					{
						f1 = blank;//set f1 equal to a blank AL 
						f1.add(new Set(1));//add 1 to indicate true
					}
					
				}
				else//if sets are not equal
				{
					f1 = blank;//set f1 equal to a blank AL
					f1.add(new Set(0));//add 0 to indicate false
				}
				
				moreEQ = 1;
			}
			
		return f1;//return array list containing the newly parsed variables
	}
	
	//converts to a pair structure
	private static Pair2 [] pairArguments(ArrayList <Set> p2)
	{
		ArrayList<Set> gh = p2;//stores inputed array list
		Pair2 [] tup = new Pair2 [gh.size()];//creates a new array the same size as the AL
		
		for(int hq = 0; hq < gh.size();hq++)//takes each element of the AL and adds it to the pair array 
		{
			if(gh.get(hq).getType().equals("set"))
			{
				tup[hq] = (new Pair2(gh.get(hq).getSet()));
			}
			
			if(gh.get(hq).getType().equals("pair2"))
			{
				tup[hq] = (new Pair2(gh.get(hq).getPair2()));
			}
			
			if(gh.get(hq).getType().equals("int"))
			{
				tup[hq] = (new Pair2(gh.get(hq).getInt()));
			}
		}
		
		return tup;//returns the pair (tuple)
		
	}
	// method to test set membership
	private static Boolean setMembership(ArrayList <Set> set1, ArrayList <Set> set2)
	{
		ArrayList <Set> a = set1;
		ArrayList <Set> b = set2;
		//System.out.println(a.size() + "s1 size");
		//System.out.println(b.size() + "s2 size");
		//System.out.println(a.get(0).getType() + "a");
		//System.out.println(b.get(1).getType() + "a");
		int fl = 0;
		
		for(int s = 0; s < a.size(); s++)
		{
			//compares each element of a against b if both are set compare them further
				if(a.get(s).getType().equals("set"))
				{
					for(int y = 0; y < b.size(); y++)
					{
						if(b.get(y).getType().equals("set"))
						{
							//System.out.println("set");
							ArrayList <Set> f = a.get(s).getSet();
							ArrayList <Set> g = b.get(y).getSet();
							if(setMembership(f,g) == true)//take the two elements and compare them further using recursion
							{//if they are the same add 1 to the flag
								fl++;
								//System.out.println("set");
							}
						}
					}
				}
				//compares each element of a against b if both are pairs compare them further
				if(a.get(s).getType().equals("pair2"))
				{
					for(int y = 0; y < b.size(); y++)
					{
						if(b.get(y).getType().equals("pair2"))
						{
							Pair2 [] f = a.get(s).getPair2();
							Pair2 [] g = b.get(y).getPair2();
							if(pairMembership(f,g) == true)//if both are pairs check them in pairMembership
							{//if true add one to flag
								fl++;
							}
						}
					}
				}
				//compare each element of a to each element of b 
				if(a.get(s).getType().equals("int"))
				{
					//if both are ints then compare them
					for(int y = 0; y < b.size(); y++)
					{
						if(b.get(y).getType().equals("int"))
						{
							
							int r = a.get(s).getInt();
							int q = b.get(y).getInt();
							if(q == r)//if the integers are the same add one to flag
							{
								fl++;
								//System.out.println("int");
							}
						}
					}
				}
		}
		if(a.size() != 0 && b.size() != 0)//if a and b are size zero exit, if not check flag
		{
			if(fl == a.size())//if the flag is the same size as a then a is in b
			{
				return true;
			}
			else//if flag is not the same size then a is not in b
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
		
		
	}
	//separate method to compare pairs - runs normal method as well 
	private static Boolean pairMembership(Pair2 [] pair1, Pair2 [] pair2)
	{
		Pair2 [] a = pair1;
		Pair2 [] b = pair2;
		int fl = 0;
		
		//takes the elements of both arrays and compares them, works the same way as the normal method
		for(int i = 0; i < a.length;i++)
		{
			if(a[i].getType().equals("set"))
			{
				if(b[i].getType().equals("set"))
				{
					ArrayList <Set> f = a[i].getSet();
					ArrayList <Set> g = b[i].getSet();
					if(setMembership(f,g) == true)
					{
						fl++;
					}
				}
			}
		}
		
		for(int i = 0; i < a.length;i++)
		{
			if(a[i].getType().equals("pair2"))
			{
				if(b[i].getType().equals("pair2"))
				{
					Pair2 [] f = a[i].getPair2();
					Pair2 [] g = b[i].getPair2();
					if(pairMembership(f,g) == true)
					{
						fl++;
					}
				}
			}
		}
		
		for(int i = 0; i < a.length;i++)
		{
			if(a[i].getType().equals("int"))
			{
				if(b[i].getType().equals("int"))
				{
					int f = a[i].getInt();
					int g = b[i].getInt();
					if(f == g)
					{
						fl++;
					}
				}
			}
		}
			
		
		
		if(fl == a.length)
		{
			return true;
		}
		else
		{
			return false;
		}
		
		
	}
	//compares two sets too see if they are the same
	private static Boolean setEquality(ArrayList <Set> set1, ArrayList <Set> set2)
	{
		ArrayList <Set> a = set1;
		ArrayList <Set> b = set2;
		int fl = 0;
		//System.out.println(a.size() + "s1 size");
		//System.out.println(b.size() + "s2 size");
		//System.out.println(a.get(0).getType() + "a");
		//System.out.println(a.get(1).getType() + "a");
		
		if(a.size() == b.size())//if a and b are different sizes then they are not the same
		{
			//System.out.println("eq1");
			for(int s = 0; s < a.size(); s++)//takes each element of a and compars it to each element of b
			{
				if(a.get(s).getType().equals("set"))
				{
					for(int y = 0; y < b.size(); y++)//if both are set - the method goes deeper
					{
						if(b.get(y).getType().equals("set"))
						{
							//System.out.println("set");
							ArrayList <Set> f = a.get(s).getSet();
							//byeSets(a.get(s).getSet());
							ArrayList <Set> g = b.get(y).getSet();
							if(setEquality(f,g) == true)
							{
								fl++;
								//System.out.println(fl +"flag count");
								//System.out.println("set");
							}
						}
					}
				}
				
				if(a.get(s).getType().equals("pair2"))
				{
					for(int y = 0; y < b.size(); y++)//if both are pair then compare further
					{
						if(b.get(y).getType().equals("pair2"))
						{
							Pair2 [] f = a.get(s).getPair2();
							Pair2 [] g = b.get(y).getPair2();
							if(pairEquality(f,g) == true)//separate method for comparing pair type
							{
								fl++;
							}
						}
					}
				}
				
				if(a.get(s).getType().equals("int"))
				{
					for(int y = 0; y < b.size(); y++)//if both are integers compare them
					{
						if(b.get(y).getType().equals("int"))
						{
							int r = a.get(s).getInt();
							int q = b.get(y).getInt();
							//System.out.println(r + "r");
							//System.out.println(q +"ssssssssssss");
							if(q == r)//if the integers are the same add one to the flag
							{
								fl++;
								//System.out.println("int");
								//System.out.println(fl +"flag count");
							}
						}
					}
				}
				
			}
			
		}
		//System.out.println(fl);
		if(a.size() != 0 && b.size() != 0)//if a and b are of size zero exit
		{
			if(fl == a.size())//if the flag is the same size as a then the sets are the same
			{
				return true;
			}
			else//else they are not equal
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	
		
	}
	//separate method for pairEquality - just to make it easier to check 
	private static Boolean pairEquality(Pair2 [] pair1, Pair2 [] pair2)
	{
		Pair2 [] a = pair1;
		Pair2 [] b = pair2;
		int fl = 0;
		
		if(a.length == b.length)//takes both arrays and compares them if they are the same size
		{
			for(int i = 0; i < a.length;i++)//as tuples are orderd compare the element in the same position 
			{								//in both arrays
				if(a[i].getType().equals("set"))
				{
					if(b[i].getType().equals("set"))//if both are sets compare further
					{
						ArrayList <Set> f = a[i].getSet();
						ArrayList <Set> g = b[i].getSet();
						if(setEquality(f,g) == true)//if they are the same add one to flag
						{
							fl++;
						}
					}
				}
			}
			
			for(int i = 0; i < a.length;i++)
			{
				if(a[i].getType().equals("pair2"))
				{
					if(b[i].getType().equals("pair2"))//if both are pairs compare further
					{
						Pair2 [] f = a[i].getPair2();
						Pair2 [] g = b[i].getPair2();
						if(pairEquality(f,g) == true)//if they are the same add one to flag
						{
							fl++;
						}
					}
				}
			}
			
			for(int i = 0; i < a.length;i++)
			{
				if(a[i].getType().equals("int"))//if both are integers then compare them
				{
					if(b[i].getType().equals("int"))
					{
						int f = a[i].getInt();
						int g = b[i].getInt();
						if(f == g)//if they are the same add one to flag
						{
							fl++;
						}
					}
				}
			}
			
		}
		
		if(fl == a.length)//if flag is the same length as a then return true
		{
			return true;
		}
		else//if flag is not the same size return false
		{
			return false;
		}
		
	}
		//takes the AL that have been parsed from JSON and print them
	private static void printSet(ArrayList <Set> set,BufferedWriter out) throws IOException{
			
			ArrayList <Set> set1;
			set1 = set;
			
			
			out.write('{');//prints curly brackets
			
			//takes each element in the data structure of set and prints it
			
			for(int s = 0; s < set1.size(); s++)
			{
				//System.out.println("sa");
				if(set1.get(s).getType().equals("set"))
				{
					ArrayList <Set> set2 = set1.get(s).getSet();//takes set and recurses through it printing printing brackets etc when needed
					printSet(set2,out);
					//System.out.println("sa1");
				}
				
				
				if(set1.get(s).getType().equals("pair2"))//recurses through pair structure
				{
					
					printPair2(set1.get(s).getPair2(),out);
					//System.out.println("sa2");
					
				}
				
				if(set1.get(s).getType().equals("int"))//prints the integer
				{
					//int u = set1.get(s).getInt();
					out.write(String.valueOf(set1.get(s).getInt()));
					//System.out.println(u);
					
				}
				if(set1.size() != 1 && s != set.size()-1 )
				{
					out.write(',');//prints comma
				}
			}
			
			
			out.write('}');
			
	}
	

	//print function for pair structure(tuple)
	private static void printPair2(Pair2 [] p2,BufferedWriter out) throws IOException
	{
		Pair2 [] pa2;
		pa2 = p2;
		Set[] a = new Set[1];

		
		//takes each element of the tuple recurses through it and prints the required items
		out.write('(');
		for(int y = 0; y < pa2.length;y++)
		{			
			if(pa2[y].getType().equals("set"))
			{
				ArrayList <Set> fs = pa2[y].getSet();
				printSet(fs,out);//if a set run through print set
			}
			
			if(pa2[y].getType().equals("pair2"))
			{
				Pair2 [] fs = pa2[y].getPair2();
				printPair2(fs,out);
			}
			
			if(pa2[y].getType().equals("int"))
			{
				
				out.write(String.valueOf(pa2[y].getInt()));//prints the integer
				
			}
			if(y != pa2.length-1)
			{
				out.write(',');
			}
		}
		
		out.write(')');
	}
	
	
	
	public static void main(String args[]) throws IOException , JSONException
	{
		//parses the json file through the json tokener
		BufferedReader input = new BufferedReader(new FileReader("input.json"));
		JSONTokener json = new JSONTokener(input);
		JSONObject jobj = new JSONObject(json);//stores the output from the tokener as a JSONObject
		String aj = "";//string for argument name
		JSONArray var = new JSONArray();//array to store each element in statement list
		
		var = (JSONArray) jobj.get("statement-list");//puts the statement list as a json array into var
		//aj = var.getString(0);
       // System.out.println(var);
        
        int size = var.length();
        ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();//takes each element of statement list and stores it in the array list
        for (int i = 0; i < size; i++) {
            JSONObject another_json_object = var.getJSONObject(i);
            //System.out.println(another_json_object);
                //Blah blah blah...
                arrays.add(another_json_object);
        }
        
        input.close();//closes input stream
   
	    JSONObject[] jsons = new JSONObject[arrays.size()];
	    arrays.toArray(jsons);
	    try
	    {
	    	//creates writer to output to file
		    BufferedWriter out = new BufferedWriter((new FileWriter("out.txt")));
		    
		    for(int t = 0; t < arrays.size();t++)
		    {
		    	String id = "";//string to store variable name for array list
		    	if(arrays.get(t).getString("operator").equals("equal"))
		    	{
		    		
		    		id = arrays.get(t).getJSONArray("arguments").getJSONObject(0).getString("variable");
		    		//gets the variable name for this argument ie the variable for the structure to be created
		    	}
		    	else//if there is no variable name for answer output error and break
		    	{
		    		out.write("\n");
		    		out.write("Bad JSON Input - no equals operator");
		    		break;
		    	}
		    	flag = 0;//sets flags to zero
		    	undefined = 0;
		    	moreEQ = 0;
		    	opflag = 0;
		    	JSONArray f = arrays.get(t).getJSONArray("arguments");
		    	ArrayList <Set> hi = arguments(f);//parses the json array and stores the information in the data structure
		    	answers.add(new Name(id,hi));//adds the answer to an array so it can be retrieved if needed in another answer + printed
		    	if(undefined == 1)//if a variable is undefined print undefined then stop
	    		{
	    			out.write("undefined variable in json file");
	    			//answers.remove(t);
	    			//break;
	    			
	    			
	    		}
		    	if(opflag == 4)//if there is no known operator then print bad input - then break
		    	{
		    		out.write("Bad JSON Input");
		    		answers.remove(t);
	    			break;
	    			
		    	}
		    	//depending on type of element in the array list print out accordingly
			    	if(answers.get(t).getB().get(0).getType().equals("set"))
		    		{
		    			out.write(answers.get(t).getType() + " " + "=" + " ");
		    			printSet(answers.get(t).getB().get(0).getSet(),out);
		    			
		    			
		    		}
		    		if(answers.get(t).getB().get(0).getType().equals("int"))
		    		{
		    			out.write(answers.get(t).getType() + " " + "=" + " ");
		    			out.write(String.valueOf(answers.get(t).getB().get(0).getInt()));
		    			
		    		}
		    		if(answers.get(t).getB().get(0).getType().equals("pair2"))
		    		{
		    			out.write(answers.get(t).getType() + " " + "=" + " ");
		    			printPair2(answers.get(t).getB().get(0).getPair2(),out);
		    			
		    		}
		    		out.write("\n");//newline
		    	}
	    		
	    		
			   // System.out.println(hi + "dfdf");
		    
	    	
	    	out.close();//close the output
    		
	    }
	    catch (IOException e)
	    {//Catch exception if any
	    	System.err.println("Error: " + e.getMessage());
	    }
	    catch (JSONException e)
	    {//Catch exception if any
	    	System.err.println("Error: " + e.getMessage());
	    }
	    }
	}

	
	

