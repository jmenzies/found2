import java.util.*;
import java.lang.*; 
public class par1 {
	
	private static void printSet(ArrayList <Set> set){
		
		ArrayList <Set> set1;
		set1 = set;
		
		if(set1.size() != 1)
		{
			System.out.print('{');
		}
		for(int s = 0; s < set1.size(); s++)
		{
			if(set1.get(s).getType().equals("set"))
			{
				ArrayList <Set> set2 = set1.get(s).getSet();
				printSet(set2);
			}
			
			
			if(set1.get(s).getType().equals("pair2"))
			{
				
				printPair2(set1.get(s).getPair2());
				
			}
			
			if(set1.get(s).getType().equals("int"))
			{
				System.out.print(set1.get(s).getInt());
				
			}
			if(set1.size() != 1 && s != set.size()-1 )
			{
				System.out.print(',');
			}
		}
		
		if(set1.size() != 1)
		{
			System.out.print('}');
		}
		
		
	}
	

	
	private static void printPair2(Pair2 [] p2)
	{
		Pair2 [] pa2;
		pa2 = p2;
		Set[] a = new Set[1];

		
		
		System.out.print('(');
		for(int y = 0; y < pa2.length;y++)
		{			
			if(pa2[y].getType().equals("set"))
			{
				ArrayList <Set> fs = pa2[y].getSet();
				printSet(fs);
			}
			
			if(pa2[y].getType().equals("pair2"))
			{
				Pair2 [] fs = pa2[y].getPair2();
				printPair2(fs);
			}
			
			if(pa2[y].getType().equals("int"))
			{
				ArrayList <Set> fs = new ArrayList <Set>();
				fs.add(new Set(pa2[y].getInt()));
				printSet(fs);
			}
			if(y != pa2.length-1)
			{
				System.out.print(',');
			}
		}
		
		System.out.print(')');
	}
	
	private static Set[] Union(Set[] a, Set [] b)
	{
		// creates sets for input and a new set for the output
		Set[] lh;
		Set[] rh;
		lh = a;
		rh = b;
		Set[] x4 = new Set[3];
		//position marker
		int pos = 0;
		//adds all elements of first set to the new set
		for(int g = 0; g < lh.length; g ++)
		{
			if(lh[g].getType().equals("set"))
			{
				x4[pos] = new Set(lh[g].getSet());
				pos = pos + 1;
				
			}
			
			if(lh[g].getType().equals("pair"))
			{
				x4[pos] = new Set(lh[g].getPair());
				pos = pos + 1;
				
			}
			
			if(lh[g].getType().equals("pair2"))
			{
				x4[pos] = new Set(lh[g].getPair2());
				pos = pos + 1;
				
			}
			
			
		}
		//adds all elements from second set that arent already in the new set to it
		//however this only works for this set as i havent had time to complete the validation loops
		for(int f = 0; f < rh.length; f++)
		{
			int in = 0;
			for(int h = 0;h < x4.length; h++)
			{
				if(rh[f].equals(x4[h]))
				{
					in = 1;
					
				}
			}
			
			if(in == 0)
			{
				if(rh[f].getType().equals("set"))
				{
					x4[pos] = new Set(rh[f].getSet());
					pos = pos + 1;
					
				}
				
				if(rh[f].getType().equals("pair"))
				{
					x4[pos] = new Set(rh[f].getPair());
					pos = pos + 1;
					
				}
				
				if(rh[f].getType().equals("pair2"))
				{
					x4[pos] = new Set(rh[f].getPair2());
					pos = pos + 1;
					
				}
			}
		}
		//returns set
		return x4;
		
	}
	
	private static Set[] Difference(Set[] a, Set [] b)
	{
		// creates sets to store input and a new to put the new set into
		Set[] lh ;
		Set[] r ;
		Set[] rh = new Set[1];
		lh = a;
		r = b;
		rh[0] = new Set(r);
		Set[] x5  = new Set[2];
		//sets a position marker for the new set
		int pos = 0;
		
		for(int g = 0; g < lh.length; g++)
		{
			// if the element in the left hand side is a set and any of the elements
			// in the right hand side is a set then compare them 
			if(lh[g].getType().equals("set"))
			{
				int in1 = 0;
				for(int z = 0; z < rh.length;z++)
				{
				
					if(rh[z].getType().equals("set"))
					{
						
						if(lh[g].getSet() == (rh[0].getSet()))
						{
							in1 = in1 +1;
							
						}
					}
				
				}
				// if lh side is not in rh side then put it into the new set
				if(in1 == 0)
				{
					x5[pos] = new Set(lh[g].getSet());
					pos++;
				}
				
				
			}
			// if the element in the left hand side is a pair and any of the elements
			// in the right hand side is a pair then compare them 
			if(lh[g].getType().equals("pair"))
			{
				int in2 = 0;
				for(int z = 0; z < rh.length;z++)
				{
				
					if(rh[z].getType().equals("pair"))
					{
						
						if(lh[g].getPair() == (rh[0].getPair()))
						{
							in2 = in2 +1;
							
						}
					}
				
				}
				// if lh side is not in rh side then put it into the new set
				if(in2 == 0)
				{
					x5[pos] = new Set(lh[g].getPair());
					pos++;
				}
				
				
			}
			// if the element in the left hand side is a pair2 and any of the elements
			// in the right hand side is a pair2 then compare them 
			if(lh[g].getType().equals("pair2"))
			{
				int in3 = 0;
				for(int z = 0; z < rh.length;z++)
				{
				
					if(rh[z].getType().equals("pair2"))
					{
						
						if(lh[g].getPair2() == (rh[0].getPair2()))
						{
							in3 = in3 +1;
							
						}
					}
				
				}
				// if lh side is not in rh side then put it into the new set
				if(in3 == 0)
				{
					x5[pos] = new Set(lh[g].getPair2());
					pos++;
				}
				
				
			}
				
		
		}
		//returns set
		return x5;
	}
	
	private static Set[] Intersection(Set[] a, Set [] b)
	{
		// creates sets to store input and a new to put the new set into
		Set[] lh ;
		Set[] r ;
		Set[] rh = new Set[1];
		lh = a;
		r = b;
		rh[0] = new Set(r);
		Set[] x6  = new Set[1];
		//sets a position marker for the new set
		int pos = 0;
		
		for(int g = 0; g < lh.length; g++)
		{
			// if the element in the left hand side is a set and any of the elements
			// in the right hand side is a set then compare them 
			if(lh[g].getType().equals("set"))
			{
				int in1 = 0;
				for(int z = 0; z < rh.length;z++)
				{
				
					if(rh[z].getType().equals("set"))
					{
						
						if(lh[g].getSet() == (rh[0].getSet()))
						{
							in1 = in1 +1;
							
						}
					}
				
				}
				// if lh side is in rh side then put it into the new set
				if(in1 > 0)
				{
					x6[pos] = new Set(lh[g].getSet());
					pos++;
				}
				
				
			}
			// if the element in the left hand side is a pair and any of the elements
			// in the right hand side is a pair then compare them 
			if(lh[g].getType().equals("pair"))
			{
				int in2 = 0;
				for(int z = 0; z < rh.length;z++)
				{
				
					if(rh[z].getType().equals("pair"))
					{
						
						if(lh[g].getPair() == (rh[0].getPair()))
						{
							in2 = in2 +1;
							
						}
					}
				
				}
				// if lh side is in rh side then put it into the new set
				if(in2 > 0)
				{
					x6[pos] = new Set(lh[g].getPair());
					pos++;
				}
				
				
			}
			// if the element in the left hand side is a pair2 and any of the elements
			// in the right hand side is a pair2 then compare them 
			if(lh[g].getType().equals("pair2"))
			{
				int in3 = 0;
				for(int z = 0; z < rh.length;z++)
				{
				
					if(rh[z].getType().equals("pair2"))
					{
						
						if(lh[g].getPair2() == (rh[0].getPair2()))
						{
							in3 = in3 +1;
							
						}
					}
				
				}
				// if lh side is in rh side then put it into the new set
				if(in3 > 0)
				{
					x6[pos] = new Set(lh[g].getPair2());
					pos++;
				}
				
				
			}
				
		
		}
		//returns set
		return x6;
	}
	
	
	public static void main(String args[]) {
	      // Creates and initialises Sets to store each set 
		  ArrayList <Set> f = new ArrayList <Set>() ;
		  Set[] x0 = new Set[1];
		  ArrayList <Set> f1 = new ArrayList <Set>() ;
		  ArrayList <Set> f2 = new ArrayList <Set>() ;
	      Set[] x1 = new Set[8];
	      Set[] x2 = new Set[2];
	      Set[] x3 = new Set[1];
	      Set[] x4 = new Set[3];
	      Set[] x5 = new Set[2];
	      Set[] x6 = new Set[1];
	      // Creates Pairs to store the pairs that are needed in some of the sets
	      Pair2 [] one = new Pair2 [2];
	      Pair2 two;
	      
	      x0[0] = new Set(8);
	      f.add(new Set(8));
	      
	      System.out.println(f.get(0).getInt());
	      
	      //Builds the x1 set
	      for(int d = 0; d < x1.length; d++){
	    	  
	    	  if(d < 7){
	    	  x1[d] = new Set(d+1);
	    	  f1.add(new Set(d+1));
	    	  }
	    	  else
	    		  //x1[d] = new Set(x0);
	    		  f1.add(new Set(f));
	      }
	      
	      //Builds first pair
	      ArrayList <Set> on = new ArrayList <Set>() ;
	      on.add(new Set(1));
	      one[0] = (new Pair2(1));
	      one[1] = (new Pair2(f1));
	      //Adds x1 and Pair to the set x2
	     // x2[0] = new Set(x1);
	     // x2[1] = new Set(one);
	      f2.add(new Set(f1));
	      f2.add(new Set(one));
	      
	      System.out.println(one);
	    //  two = new Pair2(x2,x1);
	      
	     // x3[0] = new Set(two);
	      // adds sets to be changed and put back into the before the =
	    //  x4 = Union(x3,x2);
	    //  x5 = Difference(x4,x1);
	    //  x6 = Intersection(x4,x1);
	      
	      // prints out all of the sets , printSet()etc puts the set throught the printmethods to be printed
	      //System.out.print("x0" + " " + " = ");
	      //printSet(x0);
	      /*System.out.println("");
	      System.out.print("x1" + " " + " = ");
	      */printSet(f1);
	     /* System.out.println("");
	      System.out.print("x2" + " " + " = ");
	     */ printSet(f2);
	    /*  System.out.println("");
	      System.out.print("x3" + " " + " = ");
	      printSet(x3);
	      System.out.println("");
	      System.out.print("x4" + " " + " = ");
	      printSet(x4);
	      System.out.println("");
	      System.out.print("x5" + " " + " = ");
	      printSet(x5);
	      System.out.println("");
	      System.out.print("x6" + " " + " = ");
	      System.out.print('{');
	      printSet(x6);
	      System.out.print('}');
	      */
	      
	      
	}
}
