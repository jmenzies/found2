import java.util.*;
public class Pair2 {
	
	Pair2 [] p2;
	ArrayList<Set> s;
	int i;
	int value = 0;
	String type = "" ;
	
	
	

	public Pair2(Pair2 [] pair2)
	{
		p2 = pair2;	
		type = "pair2";
		value = 3;
	}
	
	public Pair2(ArrayList<Set> set1)
	{
		s = set1;
		type = "set";
		value = 4;
	}
	
	public Pair2(int num)
	{
		i = num;
		type = "int";
		value = 1;
	}
	
	public int getInt()
	{
		if(value == 1)
		{
			return i;
		}
		else
			return 0;
	}
	
	public String getType()
	{
		return type;
	}
	
	
	public Pair2 [] getPair2()
	{
		if(value == 3)
		{
			return p2;
		}
		else
			return null;
	}

	public ArrayList<Set> getSet()
	{
		if(value == 4)
		{
			return s ;
		}
		else
			return null;
	}
	
	
	
}